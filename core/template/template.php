﻿<?php echo include 'includes/header.php'; ?>

<body class="theme-red" id="racineWeb" title="<?php echo $racineWeb ?>">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Chargement en cours...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Top Bar -->
    <nav class="navbar animated bounce">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?php echo $racineWeb ?>"><?php echo $siteName ?></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">                           
                    <li class="dropdown">
                        <a href="<?php echo $racineWeb ?>deconnexion" >
                            <i class="material-icons">power_settings_new</i>
                        </a>
                    </li>
                    <!-- #END# Tasks -->
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section class="">
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="<?php echo $racineWeb ?>assets/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['nom']." ".$_SESSION['prenom'] ?></div>
                    <div class="email"><?php echo $_SESSION['email'] ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<?php echo $racineWeb ?>foxusers/modifier/<?php echo $_SESSION['id'] ?>"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="<?php echo $racineWeb ?>deconnexion"><i class="material-icons">input</i>Déconnexion</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MENU PRINCIPAL</li>
                    <li class="<?php menuActif('') ?>">
                        <a href="<?php echo $racineWeb ?>">
                            <i class="material-icons">home</i>
                            <span>Accueil</span>
                        </a>
                    </li>
                    <?php echo include 'includes/menu.php'; ?>
                    
                    
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="version">
                    <b>#FOXADMIN</b> Version:  1.0  By <a href="javascript:void(0);"> #FoxY</a>.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2><?php if(isset($_GET['p'])){echo strtoupper($_GET['p']);} ?></h2>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <?php if(isset($pageTile)){echo $pageTile;} ?>
                        </h2>
                        <div class="header-dropdown m-r--5">
                            <?php if (isset($PlusBtn)): ?>
                                <?php echo $PlusBtn; ?>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="body">
                        <?php echo $content ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="table_id" data-value=""></section>

<?php include 'includes/footer.php'; ?>