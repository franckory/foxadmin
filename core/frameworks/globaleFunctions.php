<?php 

function dateRewrite($jour, $mois, $annee, $format="-"){
	if(strlen($jour)===1){
		$jour = "0".$jour;
	}
	if(strlen($mois)===1){
		$mois = "0".$mois;
	}
	return  $jour.$format.dateMois($mois).$format.$annee;
}

function dateTimeFrancais ($dateTimeFrancais){
	$dateTimeFrancais = explode(" ", $dateTimeFrancais);
	$dateFrancais     = explode("-", $dateTimeFrancais[0]);
	$date 			  = $dateFrancais[2]." ".dateMois($dateFrancais[1])." ".$dateFrancais[0];
	return  $date;
}
function dateTimeMoisFrancais ($jours, $mois, $annee){
	$date 			  = $jours." ".dateMois($mois)." ".$annee;
	return  $date;
}


function dateMois($mois){
	if($mois==1){
		return  "Janvier ";
	}if($mois==2){
		return  "Février ";
	}if($mois==3){
		return  "Mars ";
	}if($mois==4){
		return  "Avril ";
	}if($mois==5){
		return  "Mai ";
	}if($mois==6){
		return  "Juin ";
	}if($mois==7){
		return  "Juillet ";
	}if($mois==8){
		return  "Aout ";
	}if($mois==9){
		return  "Septembre ";
	}if($mois==10){
		return  "Octobre ";
	}if($mois==11){
		return  "Novembre ";
	}if($mois==12){
		return  "Décembre ";
	}
}

function voirPlus($texte, $nombre="300", $suite=" ..."){
	$NombreText = strlen($texte);
	if($nombre < $NombreText){
		$suite = $suite;
	}else{
		$suite = " ";
	}
	// dd($suite);
	$Texte = substr($texte, 0, $nombre).$suite;
	return $Texte;
}

function dd($data=null){
	var_dump($data);
	exit();
}

function findDate($date){
	$date = explode("-", $date);
	$jour = $date[2];
	$mois = $date[1];
	$annee = $date[0];

	$mois = mktime( 0, 0, 0, $mois, 1, $annee ); 
	return intval(date("t",$mois));
}

function dateCrorrecte($jour, $mois, $annee){
	$datePro = findDate(date("Y-m-").$jour);
	if($jour<=$datePro){
		return $annee."-".$mois."-".$jour;
	}else{
		$dateJour = $jour - $datePro;
		$mois = $mois + 1;
		if($mois>=12){
			$mois = $mois - 12;
			$annee = $annee + 1;
		}

		return $annee."-".$mois."-".$jour;
	}
}

function dateCrorrecteMois($jour, $mois, $annee){
	$datePro = findDate(date("Y-m-").$jour);
	if($mois>12){
		$mois = $mois - 12;
		$annee = $annee + 1;
	}
	return $annee."-".$mois."-".$jour;
	
}

function moisSansHeure($date){
	$dateTimeFrancais = explode(" ", $date);
	$dateFrancais     = explode("-", $dateTimeFrancais[0]);
	return $dateFrancais[1]."-".$dateFrancais[1]."-".$dateFrancais[0];	
}

function differenceDate($date1, $date2){
	$datetime1 = date_create($date1);
	$datetime2 = date_create($date2);
	$interval = date_diff($datetime1, $datetime2);
	return  $interval->format('%r%a');
}