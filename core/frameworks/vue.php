<?php 
require_once 'configuration.php';
/**
* 
*/
class Vue
{
	private $page;
	private $action;
	
	function __construct()
	{
		$this->racineWeb = Configuration::get('racineWeb');
		$this->title = Configuration::get('title');
		$this->debug = Configuration::get('debug');
		$this->siteName = Configuration::get('siteName');
	}

	public function getPage(){
		if(isset($_GET['p']) && !empty($_GET['p'])){
			$page = $_GET['p'];
		}else{
			$page = "accueil";
		}

		return $page;
	}

	public function getAction(){
		if(isset($_GET['action']) && !empty($_GET['action'])){
			$action = $_GET['action'];
		}else{
			$action = "index";
		}

		return $action;
	}


	public function genererVue($donnees=null, $template='template', $action=null){

		if(!is_null($donnees) && is_array($donnees) ){
			extract($donnees);
		}
		// dd($donnees);

		if(is_null($action)){
			$action = $this->getAction();
		}
		$page = $this->getPage();

		ob_start();
		$racineWeb = $this->racineWeb;
		$siteName =$this->siteName;
		if(!isset($title)){
			$title     = $this->title;
		}else{
			$title =  $this->title." : ".$title;
		}
		if(file_exists("vues/".$page."/".$action.".php")) {
			require("vues/".$page."/".$action.".php");
			$content = ob_get_clean(); 
			require_once 'vues/'.$template.'.php';
		}else {
			$this->erreurDisplay("Le fichier : vues/".$page."/".$action.".php n'existe pas");
		}
		
	}

	public function rediriger($controleur=null, $action = null){
	    if($controleur===NULL){
	      header("Location:".$this->racineWeb);
	    }else
	    {
	      header("Location:" . $this->racineWeb . $controleur . "/" . $action);
	    }
  	}

  	/**
	*  
	*/
  	public function erreurDisplay($erreur, $true=false){
  		$racineWeb = $this->racineWeb;
		$title     = $this->title;
  		if ($this->debug== 1) {
  			$erreur = $erreur;
  		}elseif ($this->debug!= 1 && $true==true) {
  			$erreur = $erreur;
  		}else{
  			$erreur = "Page introuvable";
  		}
  		require_once 'vues/erreur.php';
  	}
}