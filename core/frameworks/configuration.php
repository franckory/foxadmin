<?php 

class Configuration{


	public static function get($nom, $valeurdefaut=null){
		if(file_exists("configurations/dev.ini")){
			$ini_array = parse_ini_file("configurations/dev.ini");
			if(isset($ini_array[$nom])){
				return $ini_array[$nom];
			}else{
				exit();
			}

		}else{
			throw new Exception("Aucun fichier de configuration");	
		}
	}

	public static function developpement(){
		$debug = Configuration::get('debug');
		if($debug==1){
			error_reporting(E_ALL);
		}else{
			error_reporting(0);
		}
	}
}