<?php 
/**
* 
*/
class oublieControleur extends Routeur
{
	
	private $Vue;
	function __construct()
	{
		$this->Vue = new Vue;
		$this->Modele = new Oublie;
		$this->utilisateur = new Utilisateur;

	}

	public function index(){
		$this->utilisateur->isNotConnected();
		return $this->Vue->genererVue(array(), 'login');
	}

	public function recuperation(){
		if(isset($_POST['email']) && !empty($_POST['email']) ){
		// dd($_POST);
			if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
				$error['error']['champ'] = 'email';
				$error['error']['text'] = "Veuillez saisir une adresse mail valide";
				return $this->Vue->genererVue($error, 'login', 'index');
				return $this->Vue->genererVue($error, 'login', 'index');
			}
			$recuperation = $this->Modele->recuperation($_POST['email']);
			// dd($recuperation);
			if(is_bool($recuperation) && $recuperation==true){
				$success['success']['champ'] = 'email';
				$success['success']['text'] = 'Nous vous avons envoyé un mot de passe à votre adresse email';
				return $this->Vue->genererVue($success, 'login', 'index');
			}else{
				$error['error']['champ'] = 'email';
				$error['error']['text'] = $recuperation;
				return $this->Vue->genererVue($error, 'login', 'index');
			}
		}
	}
}

?>