<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>#FoxADMIN</title>

        <!-- Google Font -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
		<!-- BootStrap Stylesheet -->
        <link rel="stylesheet" href="assets/external/bootstrap/css/bootstrap.min.css">
		<!-- Font-Awesome Stylesheet -->
        <link rel="stylesheet" href="assets/external/font-awesome/css/font-awesome.min.css">
        <!-- Sweet Alert -->
        <link rel="stylesheet" type="text/css" href="assets/sweetalert/sweetalert.css">
		
		
		<!-- Plugin Custom Stylesheet -->
		<link rel="stylesheet" href="assets/custom/css/form-wizard-orange.css">
		<link href="switcher/switcher.css" rel="stylesheet">
		<style type="text/css">
			body{
				background: #ff5722;
			}
		</style>
    </head>

    <body oncontextmenu="return false;">

        <!-- main content -->
        <section class="form-box">
            <div class="container">

                
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
					
						<!-- Form Wizard -->
						<div class="form-wizard form-header-modarn form-body-material">
						<!-- 
						Just change the class name for make it with different style of design.

						Use anyone class "form-header-classic" or "form-header-modarn" or "form-header-stylist" for set your form header design.
						
						Use anyone class "form-body-classic" or "form-body-material" or "form-body-stylist" for set your form element design.
						-->
						
                    	<form role="form" action="" method="post">

                    		<h3>#Fox<b>ADMIN</b></h3>
                    		<p>Développer une administration plus facilement !</p>
							
							<!-- Form progress -->
                    		<div class="form-wizard-steps form-wizard-tolal-steps-4">
                    			<div class="form-wizard-progress">
                    			    <div class="form-wizard-progress-line" data-now-value="12.25" data-number-of-steps="4" style="width: 12.25%;"></div>
                    			</div>
								<!-- Step 1 -->
                    			<div class="form-wizard-step active">
                    				<div class="form-wizard-step-icon"><i class="fa fa-database" aria-hidden="true"></i></div>
                    				<p>Serveur</p>
                    			</div>
								<!-- Step 1 -->
								
								<!-- Step 2 -->
                    			<div class="form-wizard-step">
                    				<div class="form-wizard-step-icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                    				<p>Compte</p>
                    			</div>
								<!-- Step 2 -->
								
								<!-- Step 3 -->
								<div class="form-wizard-step">
                    				<div class="form-wizard-step-icon"><i class="fa fa-cogs" aria-hidden="true"></i></div>
                    				<p>Paramètres</p>
                    			</div>
								<!-- Step 3 -->
								
								<!-- Step 4 -->
								<div class="form-wizard-step">
                    				<div class="form-wizard-step-icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                    				<p>Success</p>
                    			</div>
								<!-- Step 4 -->
                    		</div>
							<!-- Form progress -->
                    		
							
							<!-- Form Step 1 -->
                    		<fieldset>
								<!-- Progress Bar -->
								<div class="progress">
								  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%">
								  </div>
								</div>
								<!-- Progress Bar -->
                    		    <h4>Information serveur: <span>Etape 1 - 4</span></h4>
                    			<div class="form-group">
                    			    <label>Serveur: <span>*</span></label>
                                    <input type="text" name="serveur" placeholder="Serveur" class="form-control required">
                                </div>
                                <div class="form-group">
                    			    <label>Utilisateur: <span>*</span></label>
                                    <input type="text" name="utilisateur" placeholder="Utilisateur" class="form-control required">
                                </div>
								<div class="form-group">
                    			    <label>Mot de passe: <span>*</span></label>
                                    <input type="password" name="pass" placeholder="Mot de passe" class="form-control required">
                                </div>
								<div class="form-group">
                    			    <label>Baase de données: <span>*</span></label>
                                    <input type="text" name="database" placeholder="Base de données" class="form-control required">
                                </div>
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-next">Suivant</button>
                                </div>
                            </fieldset>
							<!-- Form Step 1 -->

							<!-- Form Step 2 -->
                            <fieldset>
								<!-- Progress Bar -->
								<div class="progress">
								  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
								  </div>
								</div>
								<!-- Progress Bar -->
                                <h4>Information administrateur : <span>Etape 2 - 4</span></h4>
                                <div class="form-group">
                    			    <label>Nom: <span>*</span></label>
                                    <input type="text" name="nom" placeholder="Nom" class="form-control required">
                                </div>
                                <div class="form-group">
                    			    <label>Prénom: <span>*</span></label>
                                    <input type="text" name="prenom" placeholder="Prénom" class="form-control required">
                                </div>
								<div class="form-group">
                    			    <label>Email: <span>*</span></label>
                                    <input type="email" name="email" placeholder="Email" class="form-control required">
                                </div>
								<div class="form-group">
                    			    <label>Mot de passe: <span>*</span></label>
                                    <input type="password" name="mdp" placeholder="Mot de passe" class="form-control required">
                                </div>
                    			
								
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous">Précédent</button>
                                    <button type="button" class="btn btn-next">Suivant</button>
                                </div>
                            </fieldset>
							<!-- Form Step 2 -->

							<!-- Form Step 3 -->
							<fieldset>
								<!-- Progress Bar -->
								<div class="progress">
								  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">
								  </div>
								</div>
								<!-- Progress Bar -->
                                <h4>Information Projet: <span>Etape 3 - 4</span></h4>
								<div style="clear:both;"></div>
								<div class="form-group">
                    			    <label>Nom du projet: <span>*</span></label>
                                    <input type="email" name="name" placeholder="Nom du projet" class="form-control required">
                                </div>
                                <div class="form-group">
                    			    <label>Dossier du projet: <span>*</span></label>
                                    <input type="text" name="folder" placeholder="Le nom du dossier dans le www dans lequel vous avez ajouter le projet" class="form-control required">
                                </div>
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous">Précédent</button>
                                    <button type="button" class="btn btn-next">Suivant</button>
                                </div>
                            </fieldset>
							<!-- Form Step 3 -->
							
							<!-- Form Step 4 -->
							<fieldset>
								<!-- Progress Bar -->
								<div class="progress">
								  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
								  </div>
								</div>
								<!-- Progress Bar -->
                                <h4>Vérification: <span>Etape 4 - 4</span></h4>
								<div style="clear:both;"></div>
									<div class="success">
										<div class="alert alert-warning">
											Veuillez vous assurer que toutes les informations rentrées soient corretces. Si tel est le cas, veuillez Valider
										</div>					  
									</div>
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous">Précédent</button>
                                    <button type="submit" class="btn btn-submit submit">Valider</button>
                                </div>
                            </fieldset>
							<!-- Form Step 4 -->
                    	
                    	</form>
						
						</div>
						<!-- Form Wizard -->
                    </div>
                </div>
                    
            </div>
        </section>
		<!-- main content -->


        <!-- Jquery JS -->
        <script src="assets/external/jquery-1.11.1.min.js"></script>
		<!-- bootStrap JS -->
		<script src="assets/external/bootstrap/js/bootstrap.min.js"></script>
		
        <!-- Sweet Alert -->
        <script type="text/javascript" src="assets/sweetalert/sweetalert-dev.js"></script>
		
		<!-- Plugin Custom JS -->
        <script src="assets/custom/js/form-wizard.js"></script>
		<script src="switcher/switcher.js"></script>
		<script src="assets/js/foxstep.js"></script>
        <!-- Plugin Custom JS -->

    </body>

</html>