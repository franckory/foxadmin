function errorMessage(errorMessage) {
    swal({
        title: "ERREUR SURVENUE",
        text: errorMessage,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#ff5722",
        confirmButtonText: "Fermer",
    });
}
function successMessage(errorMessage) {
    swal({
        title: "Bravo",
        text: errorMessage,
        type: "success",
        showCancelButton: false,
        confirmButtonColor: "#ff5722",
        confirmButtonText: "Fermer",
    });
}


function dd(i){
	console.log(i);
}
$(".submit").click(function(){
	form = $('form').serialize();
	$.post('generate.php', form, function(data){
		dd(data);
		if(data==200){
            successMessage("Veuillez suppimer manuellement les dossiers core et foxadmin");
            setTimeout(function(){
                window.location.href="../";
            }, 2000);

		}else{
			errorMessage(data);
		}
	})
})