<?php 
require "formulaire.php";
Class Vue extends Formulaire {


	public function creerModele($table, $champs){
		$content  = "";
		$content .= "<?php \n";
		$content .= "Class ".ucfirst($table)." extends Modele { \n\n";
		$content .= $this->creerIndex($table, $champs)."  \n";
		$content .= $this->creerModifier($table, $champs)."  \n";
		$content .= "} \n ";

		return $content;
	}

	public function creerIndex($table, $champs){
		$content = "";
		$content .= '<div class="table-responsive" id="dataList">'."\n";
		$content .= $this->t1.'<table class="table table-bordered table-striped table-hover js-basic-example dataTable">'."\n";
		$content .= $this->t2."<thead>\n";
		$content .= $this->t3."<tr>\n";
		$content .= $this->t4."<th>#</th>\n";
		$TotalChamp = count($champs);
		if($TotalChamp<=3){
			for ($i=1; $i < $TotalChamp ; $i++) { 
				$content .= $this->t4."<th>";
				$content .= $champs[$i];
				$content .= "</th>\n";	
			}
		}elseif($TotalChamp >3){
			for ($i=1; $i < 3 ; $i++) { 
				$content .= $this->t4."<th>";
				$content .= ucfirst($champs[$i]);
				$content .= "</th>\n";	
			}
		}
		$content .= $this->t4.'<th>Action</th>'."\n";
		$content .= $this->t3.'</tr>'."\n";
		$content .= $this->t2.'<thead>'."\n";
		$content .= $this->t2.'<tbody>'."\n";
		$content .= $this->t3."<?php if($"."data['$table']['total']==0){ ?>\n";
		$content .= $this->t4.'<div class="alert alert-warning">'."\n";
		$content .= $this->t5.'<i class="fa fa-warning"></i>'."\n";
		$content .= $this->t5."Vous n'avez aucune entrée \n";
		$content .= $this->t4."</div> \n";
		$content .= $this->t3."<?php }else{  \n";
		$content .= $this->t3.'$'.'i=0; '."\n";
		$content .= $this->t3." foreach($"."data['$table']['data'] as $"."value){ \n";
		$content .= $this->t3.'$'.'i+=1; '."\n";
		$content .= $this->t3.'?> '."\n";
		if($TotalChamp<=3){
		$content .= $this->t3.'<tr>'."\n";
		$content .= $this->t4."<td><?php echo $"."i; ?></td>\n";
			for ($i=1; $i < $TotalChamp ; $i++) { 
				$content .= $this->t4."<td><?php echo $"."value['$champs[$i]'] ?></td>\n";	
			}
				$content .= $this->t4.'<td>'."\n";
				$content .= $this->t4.'<a href="<?php echo $'.'racineWeb.$'.'page ;?>/modifier/<?php echo $'.'value[\'id\'] ?>" title="Modifier" class="btn btn-success  waves-effect waves-light btn-primary"><i class="fa fa-edit"></i> Modifier</a>'."\n";
				$content .= $this->t4.'<a href="#" title="Supprimer" class="button btn btn-danger  waves-effect waves-light " data-type="confirm" data-id="<?php echo $'.'value[\'id\'] ?>" data-table="'.$table.'" alt="<?php echo $i ?>" ><i class="fa fa-trash"></i> Supprimer</a>'."\n";
				$content .= $this->t4.'</td>'."\n";
				$content .= $this->t3.'</tr>'."\n";
		}elseif($TotalChamp >3){
				$content .= $this->t3.'<tr>'."\n";
				$content .= $this->t4."<td><?php echo $"."i; ?></td>\n";
			for ($i=1; $i < 3 ; $i++) { 
				$content .= $this->t4."<td><?php echo $"."value['$champs[$i]'] ?></td>\n";	
			}
				$content .= $this->t4.'<td>'."\n";
				$content .= $this->t4.'<a href="<?php echo $'.'racineWeb.$'.'page ;?>/modifier/<?php echo $'.'value[\'id\'] ?>" title="Modifier" class="btn  waves-effect waves-light btn-primary"><i class="fa fa-edit"></i> Modifier</a>'."\n";
				$content .= $this->t4.'<a href="#" title="Supprimer" class="button btn  waves-effect waves-light btn-danger" data-type="confirm" data-id="<?php echo $'.'value[\'id\'] ?>" data-table="'.$table.'" alt="<?php echo $i ?>" ><i class="fa fa-trash"></i> Supprimer</a>'."\n";
				$content .= $this->t4.'</td>'."\n";
				$content .= $this->t3.'</tr>'."\n";
		}
		$content .= $this->t2.'<?php  '."\n";
		$content .= $this->t3.' } '."\n";
		$content .= $this->t3.' } '."\n";
		$content .= $this->t2.'  ?>'."\n";
		$content .= $this->t2.'</tbody>'."\n";
		$content .= $this->t1.'</table>'."\n";
		$content .= '</div>'."\n";

		return $content;
	}


	public function creerModifier($table, $champs, $type){
		$content = "";
		$content .= '<form action="<?php echo $racineWeb.$page ?>/edit" method="post" data-statut="false">'."\n";
		$TotalChamp = count($champs);
		for ($i=1; $i < $TotalChamp; $i++) { 
		
		$content .= $this->t1.'<div class="form-group ">'."\n";
		$content .= $this->t2.'<div class="form-line ">'."\n";
		$content .= $this->generationDeChamp($table, $champs[$i], $type[$i], "modifier" );
		$content .= $this->t2."</div>\n";
		$content .= $this->t1."</div>\n";
		}
		$content .= $this->t1.'<input type="hidden" name="id" value="<?php echo $'.$table.'[\'data\'][\'id\']; ?>">'."\n";;
		$content .= $this->t1.'<button type="submit" class="btn btn-primary m-t-15 waves-effect">Valider</button>'."\n";
		$content .= "</form>\n";

		return $content;

	}

	public function creerNouveau($table, $champs, $type){
		$content = "";
		$content .= '<form action="<?php echo $racineWeb.$page ?>/enregistrer" method="post" data-statut="false">'."\n";
		$TotalChamp = count($champs);
		for ($i=1; $i < $TotalChamp; $i++) { 
		
		$content .= $this->t1.'<div class="form-group form-float">'."\n";
		$content .= $this->t2.'<div class="form-line ">'."\n";
		$content .= $this->generationDeChamp($table, $champs[$i], $type[$i], "nouveau" );
		$content .= $this->t2."</div>\n";
		$content .= $this->t1."</div>\n";
		}
		
		$content .= $this->t1.'<button type="submit" class="btn btn-primary m-t-15 waves-effect">Valider</button>'."\n";
		$content .= "</form>\n";

		return $content;

	}

	public function generationDeChamp($table, $champs, $type, $action){
		$typeData = explode("(", $type);
		if($typeData[0]=="int"){
			$content = $this->input($table, $champs, 'number', '3', $action);
		}elseif($typeData[0]=="varchar"){
			if($champs=='email'){
				$text = 'email';
			}else{
				$text = 'text';
			}
			$content = $this->input($table, $champs, $text, '3', $action);
		}elseif($typeData[0]=="tinyint"){
			$content = $this->input($table, $champs, 'int', '3', $action);
		}elseif($typeData[0]=="text"){
			$content = $this->textarea($table, $champs, 'textarea', '3', $action);
		}else{
			$content = $this->input($table, $champs, 'text', '3', $action);
		}
		return $content;
	}

	public function creerMenu($table){

		if($table!="foxconfig"){	
			if($table=="foxusers"){
				$TableList = "Utilisateurs";
			}else{
				$TableList = $table;
			}
			$content  = "";
			$content .= "<li class=\"<?php menuActif('$table') ?>\">\n";
			$content .= $this->t1."<a href=\"<?php echo $"."racineWeb ?>$table\">\n";
			$content .= $this->t2."<i class='material-icons'>list</i>\n";
			$content .= $this->t2."<span>".ucfirst($TableList)."</span>\n";
			$content .= $this->t1."</a>\n";
			$content .= "</li>\n";
			return $content;
		}
	}


}


?>