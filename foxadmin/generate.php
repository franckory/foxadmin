<?php 
if(isset($_POST)){
	extract($_POST);
	$dataReturn = array();
	include 'fonctions/config.php';
	include 'fonctions/deplacerDossier.php';
	include 'fonctions/creerControleur.php';
	include 'fonctions/creerModele.php';
	include 'fonctions/creerVue.php';
	// Créer les tables (users, parametres, ) dans la base de données
	include 'fonctions/creerParam.php';


	$Controleur = new Controleur;
	$Modele = new Modele;
	$Vue = new Vue;


	$modeles = "../modeles";
	$controleurs = "../controleurs";
	$vues = "../vues";
	$configurations = "../configurations";

	// var_dump($modeles);die();

	if(!is_dir($controleurs)){
		mkdir($controleurs);
	}
	if(!is_dir($modeles)){
		mkdir($modeles);
	}
	if(!is_dir($vues)){
		mkdir($vues);
	}
	if(!is_dir($configurations)){
		mkdir($configurations);
	}

	// // Créer les dossier accueil dans les dossier MVC
	// if(!is_dir($controleurs."/accueil")){
	// 	mkdir($controleurs."/accueil");
	// }
	// if(!is_dir($modeles."/accueil")){
	// 	mkdir($modeles."/accueil");
	// }
	// if(!is_dir($vues."/accueil")){
	// 	mkdir($vues."/accueil");
	// }


	$req = $bdd->query("show tables");
	$Menu = "";
	while($data = $req->fetch())
	{
		// Récupération de toutes les tables
		$reqChamp = $bdd->query("desc ".$data[0]);
		$allChamps = array();
		$allTypes = array();

		while ($champ = $reqChamp->fetch()) 
		{
			// Récupération de tous les champs d'une table
			$allChamps[] = $champ[0];
			$allTypes[] = $champ[1];
		}
		// Créer le dossier du controleur et fIchier controleur
		if(!is_dir($controleurs."/".$data[0])){
			mkdir($controleurs."/".$data[0]);
		}
		if(file_put_contents($controleurs."/".$data[0]."/".$data[0]."Controleur.php", $Controleur->creerControleur($data[0], $allChamps))===false){
			echo "string";
			die();
		}

		// Créer le dossier du modele et le fichier modele 
		if(!is_dir($modeles."/".$data[0])){
			mkdir($modeles."/".$data[0]);
		}
		if(file_put_contents($modeles."/".$data[0]."/".$data[0].".php", $Modele->creerModele($data[0], $allChamps))===false){
			echo "string";
			die();
		}

		// Créer les pages vues
		if(!is_dir($vues."/".$data[0])){
			mkdir($vues."/".$data[0]);
		}
		// Creation des fichier de vue index()
		if(file_put_contents($vues."/".$data[0]."/index.php", $Vue->creerIndex($data[0], $allChamps))===false){
			echo "string";
			die();
		} 
		if(file_put_contents($vues."/".$data[0]."/modifier.php", $Vue->creerModifier($data[0], $allChamps, $allTypes))===false){
			echo "string";
			die();
		} 
		if(file_put_contents($vues."/".$data[0]."/nouveau.php", $Vue->creerNouveau($data[0], $allChamps, $allTypes))===false){
			echo "string";
			die();
		} 

		// génération du menu
		$Menu .= $Vue->creerMenu($data[0]);
		
	}

		if(file_put_contents("../core/includes/menu.php", $Menu)===false){
			echo "string";
			die();
		} 
	

	// Création et entregistrement des tables (users & configurations)
	$Create = new Database($bdd);
	$user  = $Create->creerUser($_POST);

	$configuration  = $Create->creerConfigurations($_POST);
	if($user==1 && $configuration==1){

		// ecrire dans le fichier config
		file_put_contents($configurations."/dev.ini", $Modele->creerConfig($folder, $name, $serveur, $utilisateur, $pass, $database));

		// déplacer les dossier du frameworks dans le dossier principal
		if(deplacerDossier($folder)===true){
			echo 200;
		}
	}else{
		echo "Une erreur est survenue";
		die();
	}


}



?>