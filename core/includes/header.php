<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo $title ?></title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo $racineWeb ?>assets/favicon.ico" type="image/x-icon">
    
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap Core Css -->
    <link href="<?php echo $racineWeb ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo $racineWeb ?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo $racineWeb ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?php echo $racineWeb ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?php echo $racineWeb ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo $racineWeb ?>assets/css/foxadmin.css" rel="stylesheet">

    <!-- Sweetalert Css -->
    <link href="<?php echo $racineWeb ?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- Bootstrap-notify Css -->
    <link href="<?php echo $racineWeb ?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />



    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo $racineWeb ?>assets/css/themes/all-themes.css" rel="stylesheet" />
</head>