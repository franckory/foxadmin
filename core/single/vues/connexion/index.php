<form id="sign_in" method="POST" action="<?php echo $racineWeb.$page ?>/login">
    <div class="msg">Veuillez vous connecter</div>
    <?php if(isset($error)){ ?>
    <div class="text-danger text-center"><?php echo $error['text']; ?></div>
    <?php } ?>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">mail</i>
        </span>
        <div class="form-line">
            <input type="email" class="form-control" name="email" placeholder="Email" required >
        </div>
    </div>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">lock</i>
        </span>
        <div class="form-line">
            <input type="password" class="form-control" name="mdp" placeholder="Mot de passe" required>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-8 p-t-5">
            <a href="<?php echo $racineWeb ?>oublie">Mot de passe oublié ?</a>
        </div>
        <div class="col-xs-4 pull-right">
            <button class="btn btn-block bg-deep-orange waves-effect" type="submit">SIGN IN</button>
        </div>
    </div>
</form>