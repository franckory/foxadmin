<?php 
require 'frameworks/utilisateur.php';
/**
* 
*/
class accueilControleur extends Routeur
{
	
	function __construct()
	{
        $this->Utilisateur = new Utilisateur;
		$this->Utilisateur->isConnected();
		$this->Vue = new Vue();
		$this->Modele = new Accueil;
	}

	public function index(){
		$accueil = $this->Modele->index();
		return $this->Vue->genererVue();
	}
}


?>
