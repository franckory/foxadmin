$(function () {
	var racineWeb = $('#racineWeb').attr("title");
    $('.button').on('click', function () {
    	var type = $(this).data('type');
    	if(type==='confirm'){
    		showConfirmMessage($(this));
    		var id = $(this).data('id');
    		var table = $(this).data('table');
    		var table_id = table+"_"+id;
    		$('#table_id').data('value', table_id);   		
    	}
    });

function bootstrapNotify(message, type){
    $.notify({
      title: '<strong>Notification : </strong>',
      icon: 'check',
      message: message
    },{
      type: type,
      animate: {
            enter: 'animated bounceInUp',
            exit: 'animated bounceOutDown'
      },
      placement: {
            from: "bottom",
            align: "right"
      },
      offset: 20,
      spacing: 10,
      z_index: 1031,});
}


function showConfirmMessage(id) {
    swal({
        title: "Confirmez-vous ?",
        text: "Voulez-vous vraiment supprimer ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui, Je veux",
        closeOnConfirm: true
    }, function () {
        deleteElement(id);
        bootstrapNotify('Element supprimé avec succès !', 'success');
    });
}
// function confirmmationFormulaire(action) {
//     swal({
//         title: "Confirmez-vous ?",
//         text: "Voulez-vous confirmer l'action ?",
//         type: "warning",
//         showCancelButton: true,
//         confirmButtonColor: "#DD6B55",
//         confirmButtonText: "Oui, Je veux",
//         closeOnConfirm: true
//     }, function () {
//         bootstrapNotify("Enregistrement effectué avec succès.", "success");
//         if(action=='form'){
// 	        setTimeout(function(){
// 	        	$("form").submit();
// 	        }, 2000);	
//         }
//     });
// }

function deleteElement(i){
		// alert(id);
        var table_id = $("#table_id").data('value');
        var dataTable = table_id.split("_");
        var table = dataTable[0];
        var id = dataTable[1];
        var parents = $(this).parents('tr:first');
        var row = i.attr("alt");

        $.post(racineWeb+"modeles/deleteTable.php",{table:table, id : id }, function(data){
            parents.fadeOut('slow', function() {$(this).remove();});
            myFunction(row);
        });
}

function myFunction(e) {
	var e = e-1;
    $("#dataList tbody tr").eq(e).remove();
}

// Pop up pour confirmation d'enregistrement
$('form').submit(function(){
	var statut = $(this).data('statut');
	if(statut==false){
		$(this).data('statut', 'true');
        bootstrapNotify("Enregistrement effectué avec succès.", "success");
        setTimeout(function(){
            $("form").submit();
        }, 2000);
        return false;
    }
})

});

