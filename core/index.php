<?php 
session_start();
// session_destroy();
require_once 'frameworks/configuration.php';
require_once 'frameworks/modele.php';
require_once 'frameworks/routeur.php';
require_once 'frameworks/globaleFunctions.php';
require_once 'frameworks/MtmlFunctions.php';
Configuration::developpement();
extract($_GET);
// dd($_SESSION);
if(isset($_GET['p']) && !empty($_GET['p'])){
	$page = $_GET['p'];
	if(isset($_GET['action']) && !empty($_GET['action']) ){

		if(isset($_GET['id']) && !empty($_GET['id'])){
			$Routeur = new Routeur($page, $action, $id);
			$Routeur->requete();
		}else
		{
			$Routeur = new Routeur($page, $action);
			$Routeur->requete();	
		}
	}else{		
		$Routeur = new Routeur($page);
		$Routeur->requete();	
	}	
}else{
	$page = 'accueil';
	$Routeur = new Routeur($page);
	$Routeur->requete();	
}

