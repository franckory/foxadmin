<?php 

Class Modele{

	private static $bdd;
	private $dsn;
	private $login;
	private $mdp;
	

	public function ExecuteRequete($sql, $param=null){
		if($param==null){
			$req = $this->getBdd()->query($sql);
		}else{
			$req = $this->getBdd()->prepare($sql);
			$req->execute($param);
		}
		return $req;
	}

	public function delete($table, $id){
		$delete = $this->ExecuteRequete("DELETE  FROM $table WHERE id = ?", array($id));
		if($delete){
			return true;
		}else{
			return false;
		}
	}

	public function getAllElements($table, $orderBy="id", $desc="ASC", $limit=null){
		$req 	= $this->ExecuteRequete("SELECT * FROM $table  ORDER BY $orderBy $desc");
		$data 	= $req->fetchAll();
		$total  = $req->rowCount();
		return compact('total', 'data') ;
	}

	public function getOneElement($table, $id){
		$req 	= $this->ExecuteRequete("SELECT * FROM $table WHERE id = ? ", array($id));
		$data 	= $req->fetch();
		$total  = $req->rowCount();
		return compact('total', 'data') ;
	}

	// SELECTIONNER DEUX TABLE
	public function getElements($table, $tableEtrangereId, $tableEtrangere){
		$req 	= $this->ExecuteRequete("SELECT * FROM $table, $tableEtrangere WHERE $table.$tableEtrangereId = $tableEtrangere.id ");
		$data 	= $req->fetchAll();
		$total  = $req->rowCount();
		return compact('total', 'data') ;
	}

	// SELECTIONNER DEUX TABLE AVEC UNE CONDITIONS
	public function getElementsID($table, $tableEtrangereId, $tableEtrangere, $id){
		$req 	= $this->ExecuteRequete("SELECT * FROM $table, $tableEtrangere WHERE $table.$tableEtrangereId = $tableEtrangere.id AND $tableEtrangere.id = ? ", array($id));
		$data 	= $req->fetchAll();
		$total  = $req->rowCount();
		return compact('total', 'data') ;
	}


	// INSERRER
	public function Create($table, $data)
    {
      	$sql = "INSERT INTO ".$table." (";
	      foreach ($data as $key => $value) {
	        $sql .= "`".$key."`, ";
	      }
	      $sql = substr($sql, 0, -2).') VALUES (';
	      foreach ($data as $key => $value) {
	        $sql .= ":".$key.", ";
	      }

	      $sql = substr($sql, 0, -2).')';
	      $resulatat = $this->ExecuteRequete($sql, $data);
	      if($resulatat){
	      	$retour = 1;
	      }else
	      {
	      	$retour = 0;
	      }
	      return $retour;
    }

    public function Update($table, $data, $id)
    {
      	$sql = "UPDATE ".$table." SET ";
	      foreach ($data as $key => $value) {
	        $sql .= "".$key." = :".$key.", ";
	      }

	      $sql = substr($sql, 0, -2).' WHERE id = '.$id;
	      $resulatat = $this->ExecuteRequete($sql, $data);
	      if($resulatat){
	      	$retour = 1;
	      }else
	      {
	      	$retour = 0;
	      }
	      return $retour;
    }

	private static function getBdd(){
		try
		{
			$dsn = Configuration::get('dsn');
			$login = Configuration::get('login');
			$mdp = Configuration::get('mdp');
			$bdd = new PDO($dsn, $login, $mdp);
		    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch (Exception $e)
		{
		        die('Erreur de connexion : ' . $e->getMessage());
		}
		return  $bdd;
	}
}
