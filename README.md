## #FoxADMIN

**#FoxADMIN** est un générateur d'administration. C'est une application qui vous permet de générer l'administration de votre application ou site en quelques minutes seulement. 
Il s'appuie sur une base de données existante pour créer des vues. Un formulaire de création, modification, suppression est généré automatiquement pour chacune des tables de votre base de données 
son installation est simple et son utilisation facile. il comprend les fonctionalités suivantes :

1. **Connexion**
2. **Déconnexion**
3. **Mot de passe oublié**
4. **Création, modification et suppression de toutes les tables de votre base de données**

---

## Prérequis

L'utilisation de #FoxAdmin requiert l'installation de :

- APACHE
- PHP
- MYSQL
- Activation de url_rewrite

---

## Installation

L'installation de **#FoxADMIN** est très simple. Il faut télécharger le dépot. Ensuite, il faut créer un dossier dans le **www**. Il faut maintenant décompresser le doosier 
téléchargé dans le nouveau dossier créé dans le **www**. Ainsi, vous devriez avoir dans ce dossier les dossiers **core et foxadmin**.
Après cela, il faut donner tous les droits au dossier créé .
Une fois tout ce-ci terminé, vous accédez à l'application via le lien

- localhost/votre_dossier/foxadmin/

Suivez les différentes étapes du formulaire et votre administration vous sera générée.

---

## Modification & Personnalisation 

Votre nouvelle administration générée suit le concept du **MVC**. 
Cependant, il n'est pas obligé de maîtriser ce concept pour modifier votre administation. Vous aurez une documentation pour améliorer ou customiser votre administration.
Les principales modifications s'effectuent dans trois dossiers principaux :

- modeles
- vues
- controleur

Une documentation comprise dans le dossier de téléchargement donne encore plus de détails sur la personnalisation et la modification de votre administration générée.

**NB: Vous pourriez me contacter pour des suggestions d'amélioration ou pour toute incompréhension. franckory2@gmail.com**