<?php 
require "frameworks/utilisateur.php";

/**
* 
*/
class connexionControleur extends Routeur
{
	private $Vue;
	function __construct()
	{
		$this->Vue = new Vue;
		$this->Modele = new Connexion;
		$this->utilisateur = new Utilisateur;

	}

	public function index(){
		$this->utilisateur->isNotConnected();
		if(isset($_GET['id']) && !empty($_GET['id'])){
			$this->Modele->Token($_GET['id']);
			$succes['succes']['text'] = "Félicitations, votre compte a été activé avec succès";
			$this->Vue->genererVue($succes, 'login');
		}else{
			$this->Vue->genererVue(array(), 'login');			
		}
		
		
	}

	public function login(){
		if(isset($_POST) && !empty($_POST)){
			extract($_POST);
			if(isset($email) && empty($email)){
				$error['error']['champ'] = 'email';
				$error['error']['text'] = 'Veuillez saisir votre email';
				return $this->Vue->genererVue($error, 'login', 'index');
			}elseif (isset($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)){
				$error['error']['champ'] = "email";
				$error['error']['text'] = "Veuillez renseigner une adresse e-mail valide";
				return $this->Vue->genererVue($error, 'login', 'index');
			}elseif (isset($mdp) && empty($mdp)) {
				$error['error']['champ'] = 'mdp';
				$error['error']['text'] = 'Veuillez saisir votre mot de passe';
				return $this->Vue->genererVue($error, 'login', 'index');
			}elseif(isset($mdp) && strlen($mdp)<4){
				$error['error']['champ'] = 'mdp';
				$error['error']['text'] = 'Veuillez saisir votre mot de passe ayant au moins 4 caractères';	
				return $this->Vue->genererVue($error, 'login', 'index');
			}else{
				$donnees = $this->Modele->login();
				if($donnees==false){
					$error['error']['text'] = "Erreur d'authentification, veuillez vérifier vos paramètres de connexion";	
				return $this->Vue->genererVue($error, 'login', 'index');
				}elseif ($donnees==true) {
						$this->Vue->rediriger();
				}elseif($donnees=="inconnu"){
						
						$error['error']['text'] = $this->Modele->MessageConnexion($donnees);;	
						return $this->Vue->genererVue($error, 'login', 'index');
				}
				
			}
		}else{


			$this->Vue->erreurDisplay("Vous n'avez pas accès à cette page", true);
		}
	}
}

 ?>