<?php 
Class Controleur{
	public $t1;
	public $t2;
	public $t3;
	public $t4;
	public $t5;
	public $t6;


	public function __construct(){

		$this->t1 = "\t";
		$this->t2 = "\t\t";
		$this->t3 = "\t\t\t";
		$this->t4 = "\t\t\t\t";
		$this->t5 = "\t\t\t\t\t";
		$this->t6 = "\t\t\t\t\t\t";
	}

	public function creerControleur($table, $champs){
		$content  = "";
		$content .= "<?php \n";
		$content .= "require 'frameworks/utilisateur.php';\n";
		$content .= "Class ".$table."Controleur extends Routeur { \n\n";
		$content .= $this->t1."public $"."racineWeb ; \n";
		$content .= $this->t1."public $"."page ;\n \n";
		$content .= $this->t1."public function __construct() {\n";
		$content .= $this->t2."$"."this->Utilisateur = new Utilisateur; \n";
		$content .= $this->t2."$"."this->Utilisateur->isConnected(); \n";
		$content .= $this->t2."$"."this->Vue = new Vue; \n";
		$content .= $this->t2."$"."this->Modele = new ".ucfirst($table)."; \n";
		$content .= $this->t2."$"."this->racineWeb = Configuration::get('racineWeb'); \n";
		$content .= $this->t2."$"."this->page = $"."_GET['p']; \n";
		$content .= $this->t1."} \n \n";
		$content .= $this->creerIndex($table, $champs)."  \n";
		$content .= $this->creerNouveau($table, $champs)."  \n";
		$content .= $this->creerEnregistrer($table, $champs)."  \n";
		$content .= $this->creerModifier($table, $champs)."  \n";
		$content .= $this->creerEdit($table, $champs)."  \n";
		$content .= "} \n";
		$content .= "} \n ";

		return $content;
	}

	public function creerIndex($table, $champs){
		$content = "";
		$content .= $this->t1."public function index(){  \n";
		$content .= $this->t2."$"."data = $"."this->Modele->index();  \n";
		$content .= $this->t2."$"."pageTile = \"Liste des ".$table." \";  \n";
		$content .= $this->t2."$"."PlusBtn = '<a href=\"'.$"."this->racineWeb.$"."this->page.'/nouveau\" class=\"btn btn-primary blue\"><i class=\"fa fa-plus text-white\"></i>Ajouter</a>';  \n";
		$content .= $this->t2."return $"."this->Vue->genererVue(compact('data', 'pageTile', 'PlusBtn')) ; \n";
		$content .= $this->t1."} \n";

		return $content;
	}

	public function creerNouveau(){
		$content = "";
		$content .= $this->t1."public function nouveau(){  \n";
		$content .= $this->t2."$"."data = $"."this->Modele->nouveau();  \n";
		$content .= $this->t2."$"."pageTile = \"Nouveau\" ;  \n";
		$content .= $this->t2."return $"."this->Vue->genererVue(compact('data', 'pageTile')) ; \n";
		$content .= $this->t1."} \n ";
		return $content;
	}

	public function creerEnregistrer(){
		$content = "";
		$content .= $this->t1."public function enregistrer(){  \n";
		$content .= $this->t2."$"."data = $"."this->Modele->enregistrer();  \n";
		$content .= $this->t2."if($"."data==1){  \n";
		$content .= $this->t3."return $"."this->Vue->rediriger($"."this->page) ; \n";
		$content .= $this->t2."}else{  \n";
		$content .= $this->t3."return $"."this->Vue->genererVue(compact('data'));  \n";
		$content .= $this->t2."}  \n";
		$content .= $this->t1."} \n";
		return $content;
	}

	public function creerModifier($table, $champs){
		$content = "";
		$content .= $this->t1."public function modifier($"."id){  \n";
		$content .= $this->t2."$".$table." = $"."this->Modele->modifier($"."id);  \n";
		$content .= $this->t2."$"."pageTile = \"Modification de ".$table." \";  \n";
		$content .= $this->t2."return $"."this->Vue->genererVue(compact('$table', 'pageTile')) ;  \n";
		$content .= $this->t1."} \n";
		return $content;

	}

	public function creerEdit($table, $champs){
		$content = "";
		$content .= $this->t1."public function edit(){  \n";
		$content .= $this->t2."$"."data = $"."this->Modele->edit();  \n";
		$content .= $this->t2."if($"."data==1){  \n";
		$content .= $this->t3."return $"."this->Vue->rediriger($"."this->page) ; \n";
		$content .= $this->t2."}else{  \n";
		$content .= $this->t3."return $"."this->Vue->genererVue(compact('data'));  \n";
		$content .= $this->t2."  \n";
		$content .= $this->t1."} \n \n";
		return $content;

	}


	


}


?>