<?php 

/**
* PAGE PRINCIPALE ROUTEUR
*/
class Routeur 
{
	public $page;
	public $action;
	public function __construct($page, $action=null, $id=null)
	{
		$this->page = $page;
		$this->action = $action;
		$this->id = $id;
		
	}
	
	public function requete(){
 		$controleur = $this->creerControleur($this->page);
     	$action = $this->creerAction($this->action);
 		$this->executerAction($this->page, $action);
	}

	public function creerControleur($page="accueil"){
		try {
			$controleur = "controleurs/".$page."/".$page."Controleur.php";
			$modele     = "modeles/".$page."/".$page.".php";
			$vue     = "vue.php";
			if($this->page=='deconnexion'){
				$this->deconnexion();
				die();
			}
			if(file_exists($controleur) && file_exists($modele)){
				require($controleur);
				require($modele);	
				require($vue);	
			}else{
				$this->afficheErreur("Le fichier ".$controleur." ou ".$modele." n'existe pas.");			
			}

		} catch (Exception $e) {
			
		}
	}

	public function creerAction($action){
		if(isset($action) && !is_null($action)){
			$action = $action;
		}else{
			$action = 'index';
		}
		return $action;
	}

	public function executerAction($controleur,$action){
		$actionControleur = $controleur."Controleur";
		if(method_exists($actionControleur, $action)){
			$aactionExecutee = new $actionControleur;
			if($this->id==null){
				$aactionExecutee->$action();
			}else{
				$aactionExecutee->$action($this->id);
			}
		}else{
			$this->afficheErreur("La methode appelée n'existe pas ");
			// header("Location:./");
			// CREER UNE CLASSE ERREUR QUI GERE LES REDICTION OU LES ERREUR A AFFICHER
			
		}
	}

	public function afficheErreur($erreur){
		require_once 'vue.php';
		$Vue = new Vue();
		$afficherVue = $Vue->erreurDisplay($erreur);
	}

	function deconnexion(){
		require_once 'utilisateur.php';
		$utilisateur = new Utilisateur;
		$utilisateur->deconnecter();
		header("location:./");
	}

}