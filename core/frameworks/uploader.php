<?php

	/**
	* 
	*/
	class Uploader
	{
		private $file;
		private $constraints;
		private $ext;

		function __construct($uploaded_file)
		{
			$this->file = $uploaded_file;
			$this->constraints = [
				"auth_ext" => ["jpg", "png", "bmp", "gif", "pdf", "doc", "docx", "odt"],
				"max_size" => 1000000,
				"error" => "no-accept"
			];
			$this->ext = explode('.', $this->file['name']);
			$this->ext = $this->ext[count($this->ext)-1];
		}

		function getExt()
		{
			return $this->ext;
		}

		function checkFile()
		{
			if ($this->file['error'] != 0 && $this->constraints['error'] == "no-accept") {
				return $this->returns(true, "Error: File contains errors !");
			}
			else if (!in_array($this->ext, $this->constraints["auth_ext"])) {
				return $this->returns(true, "Ce type de fichier n'est pas autorisé !");
			}
			else if ($this->constraints['max_size'] && $this->file['size'] > $this->constraints['max_size']) {
				return $this->returns(true, "Error: File size error !");
			}
			else {
				return $this->returns(false, "Success: File is ok !");
			}
		}

		function uploadFile($destination='./', $name=false)
		{
			$destname = $name ? $name.'.'.$this->ext : $this->file['name'];
			return move_uploaded_file($this->file['tmp_name'], $destination.'/'.$destname);
		}

		function returns($bool, $text)
		{
			return (object) [
				"error" => $bool,
				"message" => $text
			];
		}
	}