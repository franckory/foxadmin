<?php 

function modalConfirmation(){

ob_start();
?>

<div id="myconfirmaModal" class="modal fade">
	<div class="modal-dialog modal-confirm-Confirm">
		<div class="modal-content">
			<div class="modal-heade">
				<div class="icon-box">
					<i class="fa fa-question"></i>
				</div>	
				<br><div id="errortext"></div>			
				<h4 class="modal-title">Etes-vous s&ucirc;r ?</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p id="confirmationModalText"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info confirmation" id="non" data-dismiss="modal">Non merci</button>
				<button type="button" class="btn btn-danger confirmation questionConform" id="oui">Oui, je confirme</button>
			</div>
		</div>
	</div>
</div> 

<?php 
$modal = ob_get_clean();

echo $modal;
}

// ============================ SIMPLE MODALE =================================

function simpleModal(){

ob_start();
?>

<div id="simpleModal" class="modal fade">
	<div class="modal-dialog modal-confirm-Confirm">
		<div class="modal-content">
			<div class="modal-heade">
				<div class="icon-box">
					<i class="fa fa-question"></i>
				</div>				
				<h4 class="modal-title" id="simpleModalTitle"></h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p id="simpleModalText"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info modalSimple" id="non" data-dismiss="modal">Non merci</button>
				<button type="button" class="btn btn-danger modalSimple questionConform" id="oui">Oui, je confirme</button>
			</div>
		</div>
	</div>
</div> 

<?php 
$modal = ob_get_clean();

echo $modal;
}

// ============================ SIMPLE MODALE =================================


// ============================ SMS MODALE =================================

function smsModal(){

ob_start();
?>

<div id="smsModal" class="modal fade">
	<div class="modal-dialog modal-confirm-Confirm">
		<div class="modal-content">
			<div class="modal-heade">
				<div class="icon-box">
					<i class="fa fa-phone"></i>
				</div> <br>	
				<div id="errorDisplay"></div>			
				<h4 class="modal-title" id="simpleModalTitle">Entrez le numéro</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p id="simpleModalText">
					<form class="horizontal-form">
						<input type="text" id="numero" placeholder="Numéro du correspondant" class="form-control"><br>
					</form>
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info " data-dismiss="modal">Annuler</button>
				<button type="button" class="btn btn-danger smsSend " id="oui"><i class="fa fa-envelope"></i> Inviter</button>
			</div>
		</div>
	</div>
</div> 

<?php 
$modal = ob_get_clean();

echo $modal;
}

// ============================ SMS MODALE =================================




function successModal(){
	ob_start()
 ?>
<div id="myModalSuccess" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
 			<div class="modal-heade">
				<div class="icon-box">
					<i class="fa fa-check"></i>
				</div>				
				<h4 class="modal-title">Féllicitation</h4>	
			</div>
			<div class="modal-body">
				<p class="text-center" id="successModalText"></p>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success btn-block" data-dismiss="modal" class="closeSuccesModal">OK</button>
			</div>
		</div>
	</div>
</div>    
<?php 
$successModal = ob_get_clean();
echo $successModal;
}


function errorModal(){
	ob_start()
 ?>
<div id="myModalError" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
 			<div class="modal-heade">
				<div class="icon-box">
					<i class="fa fa-times"></i>
				</div>				
				<h4 class="modal-title">Désolé</h4>	
			</div>
			<div class="modal-body">
				<p class="text-center" id="errorModalText"></p>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success btn-block" data-dismiss="modal" class="closeErrorModal">OK</button>
			</div>
		</div>
	</div>
</div>    
<?php 
$errorModal = ob_get_clean();
echo $errorModal;
}



function confirmaModal(){
	ob_start()
 ?>
    
<?php 
$confirmaModal = ob_get_clean();
echo $confirmaModal;
}





 ?>