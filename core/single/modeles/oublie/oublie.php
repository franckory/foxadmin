<?php 
require 'frameworks/utilisateur.php';

/**
* 
*/
class Oublie extends Modele
{
	public function __construct(){
		$this->Utilisateur = New Utilisateur;
	}
	
	public function index(){

	}

	public function recuperation($email){
		$recuperation = $this->Utilisateur->recuperation($email);
		if(!$recuperation){
			return  "L'envoie de mail a échoué car vos paramètres smtp ne sont pas configuré en local";
		}else{
			return $recuperation;
		}

	}
}


?>