

    <!-- Jquery Core Js -->
    <script src="<?php echo $racineWeb ?>assets/plugins/jquery/jquery.min.js"></script>

    <!-- bootstrap-notify Js -->
    <script src="<?php echo $racineWeb ?>assets/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>



    <!-- Bootstrap Core Js -->
    <script src="<?php echo $racineWeb ?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo $racineWeb ?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo $racineWeb ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo $racineWeb ?>assets/plugins/node-waves/waves.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo $racineWeb ?>assets/js/admin.js"></script>
    <script src="<?php echo $racineWeb ?>assets/js/foxadmin.js"></script>
    <script src="<?php echo $racineWeb ?>assets/js/pages/ui/dialogs.js"></script>

    <!-- Demo Js -->
    <script src="<?php echo $racineWeb ?>assets/js/demo.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo $racineWeb ?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo $racineWeb ?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo $racineWeb ?>assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo $racineWeb ?>assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo $racineWeb ?>assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo $racineWeb ?>assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo $racineWeb ?>assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php echo $racineWeb ?>assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php echo $racineWeb ?>assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="<?php echo $racineWeb ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- Materialize -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.js">


    <!-- SweetAlert Plugin Js -->
    <script src="<?php echo $racineWeb ?>assets/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Ckeditor -->
    <script src="<?php echo $racineWeb ?>assets/plugins/ckeditor/ckeditor.js"></script>

    <script type="text/javascript">
        $(function(){
            $(".dataTable").DataTable();
        })
    </script>

</body>

</html>