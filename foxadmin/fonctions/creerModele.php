<?php 
Class Modele{
	public $t1;
	public $t2;
	public $t3;
	public $t4;
	public $t5;
	public $t6;


	public function __construct(){

		$this->t1 = "\t";
		$this->t2 = "\t\t";
		$this->t3 = "\t\t\t";
		$this->t4 = "\t\t\t\t";
		$this->t5 = "\t\t\t\t\t";
		$this->t6 = "\t\t\t\t\t\t";
	}

	public function creerModele($table, $champs){
		$content  = "";
		$content .= "<?php \n";
		$content .= "Class ".ucfirst($table)." extends Modele { \n\n";
		$content .= $this->creerIndex($table, $champs)."  \n";
		$content .= $this->creerNouveau()."  \n";
		$content .= $this->creerEnregistrer($table, $champs)."  \n";
		$content .= $this->creerModifier($table, $champs)."  \n";
		$content .= $this->creerEdit($table, $champs)."  \n";
		$content .= "} \n ";

		return $content;
	}

	public function creerIndex($table, $champs){
		$content = "";
		$content .= $this->t1."public function index(){  \n";
		$content .= $this->t2."$".$table." = $"."this->getAllElements('$table');  \n";
		$content .= $this->t3."return (compact('$table'));  \n";
		$content .= $this->t1."} \n \n";

		return $content;
	}

	public function creerNouveau(){
		$content = "";
		$content .= $this->t1."public function nouveau(){  \n";
		$content .= $this->t1."} \n \n";
		return $content;
	}

	public function creerEnregistrer($table, $champs){
		$content = "";
		$content .= $this->t1."public function enregistrer(){  \n";
		$content .= $this->t2."return $"."this->Create('$table', $"."_POST);  \n";
		$content .= $this->t1."} \n \n";
		return $content;
	}

	public function creerModifier($table, $champs){
		$content = "";
		$content .= $this->t1."public function modifier($"."id){  \n";
		$content .= $this->t2."$".$table." = $"."this->getOneElement('$table', $"."id);  \n";
		$content .= $this->t2."return $"."$table ;  \n";
		$content .= $this->t1."} \n \n";
		return $content;

	}

	public function creerEdit($table, $champs){
		$content = "";
		$content .= $this->t1."public function edit(){  \n";
		$content .= $this->t2."$"."id = $"."_POST['id'];  \n";
		$content .= $this->t2."unset($"."_POST['id']);  \n";
		$content .= $this->t2."return $"."this->Update('$table', $"."_POST,  $"."id) ;  \n";
		$content .= $this->t1."} \n \n";
		return $content;

	}


	public function creerConfig($folder, $name, $serveur, $utilisateur, $pass, $database){
		$content  = "";
		$content .= "; Fichier de configuration \n\n";
		$content .= "[Configuration]\n";
		$content .= "racineWeb = /".$folder."/\n";
		$content .= "title = \"".$name."\" \n";
		$content .= "siteName = ".$name."\n\n";
		$content .= "[BD]\n";
		$content .= "dsn = 'mysql:host=".$serveur.";dbname=".$database.";charset=utf8'  \n";
		$content .= "login = ".$utilisateur."\n";
		$content .= "mdp = ".$pass."\n";
		$content .= "debug = 1 \n";
		return $content;
	}

}


?>