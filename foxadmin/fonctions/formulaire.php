<?php 

/**
* 
*/
class Formulaire 
{
	public $t1;
	public $t2;
	public $t3;
	public $t4;
	public $t5;
	public $t6;


	public function __construct(){

		$this->t1 = "\t";
		$this->t2 = "\t\t";
		$this->t3 = "\t\t\t";
		$this->t4 = "\t\t\t\t";
		$this->t5 = "\t\t\t\t\t";
		$this->t6 = "\t\t\t\t\t\t";
	}
	
	public function input($table, $champs, $type, $space=null, $action="nouveau"){
		if($action=="modifier"){
			$attribut = 'value="<?php if(isset($'.$table.'[\'data\'][\''.$champs.'\'])){ echo $'.$table.'[\'data\'][\''.$champs.'\'];  } ?>"';
		}else{
			$attribut = "";
		}
		$space = 't'.$space;
		$input  = "";
		$input .= $this->$space.'<input required="" class="form-control" type="'.$type.'" id="'.$champs.'" name="'.$champs.'" '.$attribut.'  >'."\n";
		$input .= $this->$space.'<label class="form-label">'.ucfirst($champs).'</label>'."\n";

		return $input;
	}

	public function textarea($table, $champs, $type, $space=null, $action="nouveau"){
		if($action=="modifier"){
			$attribut = '<?php if(isset($'.$table.'[\'data\'][\''.$champs.'\'])){ echo $'.$table.'[\'data\'][\''.$champs.'\'];  } ?>';
		}else{
			$attribut = "";
		}
		$space = 't'.$space;
		$textarea  = "";
		$textarea .= $this->$space.'<textarea class="form-control" required name="'.$champs.'" id="'.$champs.'" >'.$attribut.'</textarea>'."\n";
		$textarea .= $this->$space.'<label class="form-label">'.ucfirst($champs).'</label>'."\n";
		return $textarea;
	}
}

?>