<?php 

/**
* 
*/
class Database 
{
	private $bdd;
	
	function __construct($bdd)
	{
		$this->bdd = $bdd;
	}

	public function creerUser ($formData){
		$req = $this->bdd->query("show tables");
		$verifUser = false;
		while ($donnees = $req->fetch()) {
			if($donnees[0]=="foxusers"){
				$verifUser = true;
			}
		}
		if($verifUser==false){
			$data = $this->bdd->query("CREATE TABLE foxusers (
				id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
				nom VARCHAR(50) NOT NULL,
				prenom VARCHAR(150) NOT NULL,
				email VARCHAR(150) NOT NULL,
				mdp VARCHAR(225) NOT NULL,
				etat tinyint(1) NOT NULL DEFAULT 1
			)  ");
			// var_dump($formData);
		}

		$usersData = array();
		$usersData['nom'] = $formData['nom'];
		$usersData['prenom'] = $formData['prenom'];
		$usersData['email'] = $formData['email'];
		$usersData['mdp'] = md5($formData['mdp']);
		return  $this->insererDonnees('foxusers', $usersData);
		
	}

	public function creerConfigurations ($formData){
		$req = $this->bdd->query("show tables");
		$verifConfig = false;
		while ($donnees = $req->fetch()) {
			if($donnees[0]=="foxconfig"){
				$verifConfig = true;
			}
		}

		if($verifConfig==false){
		$data = $this->bdd->query("CREATE TABLE foxconfig (
				id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
				name VARCHAR(50) NOT NULL,
				template VARCHAR(150) NOT NULL,
				session VARCHAR(225) NOT NULL
			)  ");
		}
		$configData = array();
		$configData['name'] = $formData['name'];
		$configData['template'] = 'template';
		$configData['session'] = $this->CreerVariableDeSession($formData);
		return  $this->insererDonnees('foxconfig', $configData);
	}

	public function insererDonnees($table, $data)
    {
    	// Supimer l'enregistrement s'il y en a
    	$this->bdd->query("DELETE FROM $table");


      	$sql = "INSERT INTO ".$table." (";
	      foreach ($data as $key => $value) {
	        $sql .= "`".$key."`, ";
	      }
	      $sql = substr($sql, 0, -2).') VALUES (';
	      foreach ($data as $key => $value) {
	        $sql .= ":".$key.", ";
	      }

	      $sql = substr($sql, 0, -2).')';
	      $req = $this->bdd->prepare($sql);
		  return $req->execute($data);
    }

    public function CreerVariableDeSession($formData){
    	$table = array();
    	$uniqid = uniqid();
    	$table['sessionName'] = $formData['folder']."_".$uniqid;
    	$table['sessionID'] = $uniqid;
    	return json_encode($table);
    }

   


}

?>