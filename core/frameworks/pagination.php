<?php 

/**
* 
*/
class Pagination
{
	private $lien;
	private $current;
	private $totalItem;
	private $totalPerPage;

	function __construct($lien, $totalItem, $totalPerPage, $current)
	{
		$this->current = $current;
		$this->totalItem = $totalItem;
		$this->totalPerPage = $totalPerPage;
		$this->lien = $lien;
	}

	public function paginate(){
		$total = ceil($this->totalItem/$this->totalPerPage);
			$ul = "<div class='text-center wb-pagination'>";
			$ul .= "<div class='inner'>";
		for($i=1; $i<=$total;$i++){
			if($i==$this->current){
				$ul .= "<a href='#' class='page active'>".$i."</a>";
			}else{
				$ul .= "<a class='page' href=".$this->lien."/".$i.">".$i."</a>";

			}
		}
		$ul .= "</div>";
		$ul .= "</div>";

		return $ul;
	}
}



 ?>