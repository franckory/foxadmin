<?php include 'includes/header.php'; ?>

<body class="four-zero-four">
    <div class="four-zero-four-container">
        <div class="error-code">404</div>
        <div class="error-message"><?php echo $erreur ?></div>
        <div class="button-place">
            <a href="<?php echo $racineWeb ?>" class="btn btn-default btn-lg waves-effect">Retourner à l'accueil</a>
        </div>
    </div>

<?php include 'includes/footer.php'; ?>