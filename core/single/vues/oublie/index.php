<form id="sign_in" method="POST" action="<?php echo $racineWeb.$page ?>/recuperation">
    <div class="msg">Mot de passe oublié</div>
    <?php if(isset($error)){ ?>
    <div class="text-danger text-center"><?php echo $error['text']; ?></div>
    <?php } ?>
    <?php if(isset($success)){ ?>
    <div class="text-danger text-center"><?php echo $success['text']; ?></div>
    <?php } ?>

    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">mail</i>
        </span>
        <div class="form-line">
            <input type="email" class="form-control" name="email" placeholder="Mot de passe" required>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4 pull-right">
            <button class="btn btn-block bg-deep-orange waves-effect" type="submit">Valider</button>
        </div>
    </div>
</form>