<?php 
// require_once "configuration.php";
/**
* 
*/
class Utilisateur extends Modele
{
	private $racineWeb ;
	public function __construct()
	{
		$this->racineWeb = Configuration::get('racineWeb');
		$this->title = Configuration::get('title');
	}

	public function deconnecter(){
		
		session_destroy();
	}

	// Fonction qui génère la variable unique se session 
	public function getSessionVariable(){
		$req  = $this->ExecuteRequete("SELECT session FROM foxconfig");
		$data = $req->fetch();
		$data = json_decode($data['session'], true);
		return $data;
	}

	public function isConnected(){
		// Récupération de va la variable unique de session
		$sessionData = $this->getSessionVariable();

		if(isset($_SESSION["'".$sessionData['sessionName']."'"])){

			extract($_SESSION);
			$req = $this->ExecuteRequete("SELECT * FROM foxusers WHERE email = ? AND mdp = ? AND etat = 1 ", array($email, $mdp));
			$total = $req->rowCount();
			$data = $req->fetch();
			if($total===1){
	
				return true;
			}else{
				
				header("location:".$this->racineWeb);
			}
		}else{
			header("location:".$this->racineWeb."connexion");
		}
	}

	public function isNotConnected(){
		$sessionData = $this->getSessionVariable();
		if(isset($_SESSION["'".$sessionData['sessionName']."'"])){
			extract($_SESSION);
			$req = $this->ExecuteRequete("SELECT * FROM foxusers WHERE email = ? AND mdp = ? AND etat = 1 ", array($email, $mdp));
			$total = $req->rowCount();
			if($total===1){
				header("Location:".$this->racineWeb);
			}else{
				return true;
			}
		}else{
			return true;
		}
	}



	public function authentification($email, $mdp){
		$req = $this->ExecuteRequete("SELECT * FROM foxusers WHERE email = '$email' AND mdp = '$mdp' AND etat = 1 ");
		$total = $req->rowCount();
		if($total===1){
		$data = $req->fetch();
			// Récupération de va la variable unique de session
			$sessionVar = $this->getSessionVariable();
			$_SESSION['id'] = $data['id'];
			$_SESSION['email'] = $data['email'];
			$_SESSION['mdp'] = $data['mdp'];
			$_SESSION['nom'] = $data['nom'];
			$_SESSION['prenom'] = $data['prenom'];
			$_SESSION["'".$sessionVar['sessionName']."'"] = $sessionVar['sessionID'];

			return true;
		}else{
			return false;
		}
	}

	public function recuperation($email){
		$req = $this->ExecuteRequete("SELECT * FROM foxusers WHERE email = '$email' ");
		$total = $req->rowCount();
		if($total<1){
			return " Cette adresse email n'existe pas dans notre base de données";
		}elseif($total>1){
			return "Il ya une erreur sur votre email, veuillez contacter l'administateur";
		}elseif($total==1){
			$mdp = $this->genererPass(8);
			$data = $req->fetch();
			$id = $data['id'];
			$_POST['mdp'] = md5($mdp);
			$reqUpdate = $this->Update('foxusers', $_POST , $id );
			if($reqUpdate==1){
				$text = "Bonjour ".$data['nom']." ".$data['prenom'].", <br>";
				$text .= "Veuillez trouver ci après votre mot de passe de connexion <br>";
				$text .= "Nouveau mot de passe : <b>".$mdp."</b> <br>";
				$text .= "<a href='#'>Se connecter</a>";
				return $this->SendSms($email, 'franckory2@gmail.com', 'Récupération de mot de passe', $text);
			}
		}
	}

	public function genererPass($pass=4){
		$data = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$data = str_shuffle($data);
		$mdp = substr($data, 0, $pass);
		return $mdp;
	}

	public function CheckCnx(){
		if(isset($_SESSION['email']) && isset($_SESSION['mdp'])){
			extract($_SESSION);
			$req = $this->ExecuteRequete("SELECT * FROM foxusers WHERE email = ? AND mdp = ? AND etat = 1 ", array($email, $mdp));
			$total = $req->rowCount();
			if($total===1){
				$this->isOnline($_SESSION['id'], true);
				return true;
			}else{
				$this->isOnline($_SESSION['id'], false);
				return false;
			}
		}else{
			return false;
		}
	}

	public function SendSms($email,$responseEmail, $subject, $text){
		$Title    = $subject;
		$headers  = "Reply-To: \"Waribana\"<info@waribana.ci>\n";
		$headers  .= "From: \"Waribana\"<info@waribana.ci>\n";
		$headers .= "Content-Type: text/html; charset=\"utf-8\"";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "X-Priority: 3\r\n";
		$body    = "";
		ob_start();
		?>
		<!DOCTYPE html>
		<html>
		<head>
		<title></title>
		</head>
		<body style="background: #F2F2F2;padding: 50px">
			<div style="background: #fff; padding: 25px; margin:auto; width: 700px">
				<div style="text-align: center;">
					<h3><?php echo $this->title ?></h3>
					<hr>
					<br>
					<div style="color: #777;font-size: 16px;line-height: 2">
						<?php echo $text ?>
					</div>
					<br>
					<hr>
				</div>
			</div>
			<div style="text-align: center;">
				<br>
				<?php echo $this->title; ?> - <?php echo date("Y"); ?>
			</div>
			<br>
		</body>
		</html>
		<?php
		$body .= ob_get_clean();
		$send = mail($email, $Title, $body, $headers);
		return $send;
	}


	public function MessageConnexion($message){
		if($message=="inconnu"){
			return "Utilisateur inconnu, veuillez vérifier vos paramètres de connexion";
		}
	}

}