<?php include 'includes/header.php'; ?>
<body class="login-page">
    <div class="login-box ">
        <div class="logo">
            <a href="javascript:void(0);" class="animated slideInLeft">#FOX<b>ADMIN</b></a>
            <small class="animated slideInRight">Bienvenu à votre administration</small>
        </div>
        <div class="card bounceInUp  animated">
            <div class="body ">
                <?php echo $content; ?>
            </div>
        </div>
        <div class="pull-right" style="margin-top: -25px; color: #555">By <a href="#" style="color: #555">#FoxY</a></div>
    </div>

<?php include 'includes/footer.php'; ?>